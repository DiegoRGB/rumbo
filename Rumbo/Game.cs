﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Rumbo.Command;

namespace Rumbo
{
    public class Game
    {
        private Board board;
        private static bool gameStarted;

        private int x = -1;
        private int y = -1;
        private Heading facing;

        public Game()
        {
            gameStarted = false;
            board = new Board();
        }

        public string NewCommand(string commandCandidate)
        {
            Command command = ParseCommand(commandCandidate);
            if (command != null)
            {
                return ExecCommand(command);
            }
            return null;
        }

        public static Command ParseCommand(string commandCandidate)
        {
            if (string.IsNullOrEmpty(commandCandidate))
            {
                return null;
            }

            // Check if game has already started
            if (!gameStarted)
            {
                // only allowed command is PLACE
                if (!commandCandidate.StartsWith(CommandType.PLACE.ToString()))
                {
                    return null;
                }
                // Check is sintax is correct for selected command
                if (!CommandPlace.CheckCommand(commandCandidate, out CommandPlace command))
                {
                    return null;
                }

                // All good, return the just created command
                gameStarted = true;
                return command;
            }
            else
            {
                // The game has started
                if (commandCandidate.StartsWith(CommandType.PLACE.ToString()))
                {
                    // Check is sintax is correct for selected command
                    if (!CommandPlace.CheckCommand(commandCandidate, out CommandPlace command))
                    {
                        return null;
                    }
                    // All good, return the just created command
                    return command;
                }
                else if (commandCandidate.StartsWith(CommandType.REPORT.ToString()))
                {
                    // Check is sintax is correct for selected command
                    if (!CommandReport.CheckCommand(commandCandidate, out CommandReport command))
                    {
                        return null;
                    }
                    // All good, return the just created command
                    return command;
                }
                else if (commandCandidate.StartsWith(CommandType.LEFT.ToString()))
                {
                    // Check is sintax is correct for selected command
                    if (!CommandLeft.CheckCommand(commandCandidate, out CommandLeft command))
                    {
                        return null;
                    }
                    // All good, return the just created command
                    return command;
                }
                else if (commandCandidate.StartsWith(CommandType.RIGHT.ToString()))
                {
                    // Check is sintax is correct for selected command
                    if (!CommandRight.CheckCommand(commandCandidate, out CommandRight command))
                    {
                        return null;
                    }
                    // All good, return the just created command
                    return command;
                }
                else if (commandCandidate.StartsWith(CommandType.MOVE.ToString()))
                {
                    // Check is sintax is correct for selected command
                    if (!CommandMove.CheckCommand(commandCandidate, out CommandMove command))
                    {
                        return null;
                    }
                    // All good, return the just created command
                    return command;
                }

                // Unknown command or bad sintax
                return null;
            }
        }

        public string ExecCommand(Command command)
        {
            switch(command.Type)
            {
                case CommandType.PLACE:
                    this.x = ((CommandPlace)command).X;
                    this.y = ((CommandPlace)command).Y;
                    this.facing = ((CommandPlace)command).Facing;
                    break;
                case CommandType.REPORT:
                    return x + "," + y + "," + facing.ToString();
                case CommandType.LEFT:
                    switch(facing)
                    {
                        case Heading.NORTH:
                            facing = Heading.WEST;
                            break;
                        case Heading.WEST:
                            facing = Heading.SOUTH;
                            break;
                        case Heading.SOUTH:
                            facing = Heading.EAST;
                            break;
                        case Heading.EAST:
                            facing = Heading.NORTH;
                            break;
                    }
                    break;
                case CommandType.RIGHT:
                    switch (facing)
                    {
                        case Heading.NORTH:
                            facing = Heading.EAST;
                            break;
                        case Heading.EAST:
                            facing = Heading.SOUTH;
                            break;
                        case Heading.SOUTH:
                            facing = Heading.WEST;
                            break;
                        case Heading.WEST:
                            facing = Heading.NORTH;
                            break;
                    }
                    break;
                case CommandType.MOVE:
                    switch(facing)
                    {
                        case Heading.NORTH:
                            if (y + 1 < Board.MAX_SIZE)
                            {
                                y++;
                            }
                            break;
                        case Heading.SOUTH:
                            if (y > 0)
                            {
                                y--;
                            }
                            break;
                        case Heading.EAST:
                            if (x + 1 < Board.MAX_SIZE)
                            {
                                x++;
                            }
                            break;
                        case Heading.WEST:
                            if (x > 0)
                            {
                                x--;
                            }
                            break;
                    }
                    break;
            }
            return null;
        }
    }
}
