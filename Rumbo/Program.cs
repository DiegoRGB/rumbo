using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rumbo
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
             * At first there are only two states to control if the game has started or not:
             *  - Initial: The robot has not been placed on the board. The only command allowed is PLACE
             *  - Started: The robot is placed somewhere on the board. All commands are accepted.
             *  
             *  Once the game has started and the robot has been placed in a valid location, then any command can arrive.
             *  We then need to parse every command and act accordingly as follows:
             *  - PLACE X,Y,F where X and Y are numbers 0-4 and F can be "NORTH", "SOUTH", "WEST", "EAST" => places the robot on the board.
             *  - REPORT => outputs the current position and facing.
             *  - MOVE => moves the robot one step forward according to it's current facing and position. If on the border, it will not move.
             *  - RIGHT => changes facing 90º clockwise.
             *  - LEFT => changed facing 90º counter-clockwise.
             *  
             *  All input and output will be from console
             *  Program will continue forever until Ctrl+C is hit.
             *  
             *  (0,0) is the SOUTH-WEST most corner.
             */


            Console.WriteLine("Welcome to the Rumbo code challenge!!!");
            Game game = new Game();

            // Loop forever
            while (true)
            {
                string commandCandidate = Console.ReadLine();
                string output = game.NewCommand(commandCandidate);
                if(!string.IsNullOrEmpty(output))
                {
                    Console.WriteLine(output);
                }
            }

        }

    }
}
