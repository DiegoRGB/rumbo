﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rumbo
{
    public abstract class Command
    {
        public enum CommandType
        {
            PLACE,
            MOVE,
            REPORT,
            LEFT,
            RIGHT
        }

        public enum Heading
        {
            NORTH,
            SOUTH,
            WEST,
            EAST
        }

        public CommandType Type { get; }

        public Command(CommandType type)
        {
            this.Type = type;
        }
    }

    public class CommandPlace : Command
    {
        public int X { get; }
        public int Y { get; }
        public Heading Facing { get; }

        public CommandPlace(int x, int y, Heading facing) : base(CommandType.PLACE)
        {
            this.X = x;
            this.Y = y;
            this.Facing = facing;
        }

        public static bool CheckCommand(string commandString, out CommandPlace command)
        {
            command = null;
            if (string.IsNullOrEmpty(commandString))
            {
                return false;
            }
            if (!commandString.StartsWith(CommandType.PLACE.ToString()))
            {
                return false;
            }
            // Check string has three parameters
            string[] split = commandString.Split(' ');
            if (split.Length != 2)
            {
                return false;
            }
            string[] splitParameters = split[1].Split(',');
            if (splitParameters.Length != 3)
            {
                return false;
            }

            // Check x and y fall inside the configured board.
            int x = -1;
            int y = -1;
            try { x = Int32.Parse(splitParameters[0]); } catch (Exception) { x = -1; }
            try { y = Int32.Parse(splitParameters[1]); } catch (Exception) { y = -1; }

            if (x < 0 || x >= Board.MAX_SIZE || y < 0 || y >= Board.MAX_SIZE)
            {
                return false;
            }
            string facing = splitParameters[2];
            if (facing != Heading.NORTH.ToString() &&
                facing != Heading.SOUTH.ToString() &&
                facing != Heading.WEST.ToString() &&
                facing != Heading.EAST.ToString())
            {
                return false;
            }

            // It is all good, lets create the new command
            Enum.TryParse(facing, out Heading heading);
            command = new CommandPlace(x, y, heading);
            return true;
        }
    }

    public class CommandReport : Command
    {
        public CommandReport() : base(CommandType.REPORT)
        {
        }

        public static bool CheckCommand(string commandString, out CommandReport command)
        {
            command = null;
            if (string.IsNullOrEmpty(commandString))
            {
                return false;
            }
            if(!commandString.StartsWith(CommandType.REPORT.ToString()))
            {
                return false;
            }
            // Check string has no parameters
            string[] split = commandString.Split(' ');
            if (split.Length != 1)
            {
                return false;
            }
            // It is all good, lets create the new command
            command = new CommandReport();
            return true;
        }
    }

    public class CommandLeft : Command
    {
        public CommandLeft() : base(CommandType.LEFT)
        {
        }

        public static bool CheckCommand(string commandString, out CommandLeft command)
        {
            command = null;
            if (string.IsNullOrEmpty(commandString))
            {
                return false;
            }
            if (!commandString.StartsWith(CommandType.LEFT.ToString()))
            {
                return false;
            }
            // Check string has no parameters
            string[] split = commandString.Split(' ');
            if (split.Length != 1)
            {
                return false;
            }

            // It is all good, lets create the new command
            command = new CommandLeft();
            return true;
        }
    }

    public class CommandRight : Command
    {
        public CommandRight() : base(CommandType.RIGHT)
        {
        }

        public static bool CheckCommand(string commandString, out CommandRight command)
        {
            command = null;
            if (string.IsNullOrEmpty(commandString))
            {
                return false;
            }
            if (!commandString.StartsWith(CommandType.RIGHT.ToString()))
            {
                return false;
            }
            // Check string has no parameters
            string[] split = commandString.Split(' ');
            if (split.Length != 1)
            {
                return false;
            }

            // It is all good, lets create the new command
            command = new CommandRight();
            return true;
        }
    }

    public class CommandMove : Command
    {
        public CommandMove() : base(CommandType.MOVE)
        {
        }

        public static bool CheckCommand(string commandString, out CommandMove command)
        {
            command = null;
            if (string.IsNullOrEmpty(commandString))
            {
                return false;
            }
            if (!commandString.StartsWith(CommandType.MOVE.ToString()))
            {
                return false;
            }
            // Check string has no parameters
            string[] split = commandString.Split(' ');
            if (split.Length != 1)
            {
                return false;
            }

            // It is all good, lets create the new command
            command = new CommandMove();
            return true;
        }
    }
}
