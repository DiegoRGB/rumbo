﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rumbo;

namespace Test
{
    [TestClass]
    public class Move
    {
        [TestMethod]
        public void Command_UNK()
        {
            string commandStr = "AAA";
            bool res = CommandMove.CheckCommand(commandStr, out CommandMove command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR()
        {
            string commandStr = "MOVE 6";
            bool res = CommandMove.CheckCommand(commandStr, out CommandMove command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Sintax_OK()
        {
            string commandStr = "MOVE";
            bool res = CommandMove.CheckCommand(commandStr, out CommandMove command);
            Assert.AreEqual(true, res);
        }
    }
}
