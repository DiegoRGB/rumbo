﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rumbo;

namespace Test
{
    [TestClass]
    public class GameTest
    {
        [TestMethod]
        public void Command_UNK()
        {
            string commandStr = "AAA";
            Game game = new Game();
            string res = game.NewCommand(commandStr);
            Assert.AreEqual(null, res);
        }

        [TestMethod]
        public void Move_NotStarted()
        {
            string commandStr = "MOVE";
            Game game = new Game();
            string res = game.NewCommand(commandStr);
            Assert.AreEqual(null, res);
        }

        [TestMethod]
        public void Left_NotStarted()
        {
            string commandStr = "LEFT";
            Game game = new Game();
            string res = game.NewCommand(commandStr);
            Assert.AreEqual(null, res);
        }

        [TestMethod]
        public void Right_NotStarted()
        {
            string commandStr = "RIGHT";
            Game game = new Game();
            string res = game.NewCommand(commandStr);
            Assert.AreEqual(null, res);
        }

        [TestMethod]
        public void Report_NotStarted()
        {
            string commandStr = "REPORT";
            Game game = new Game();
            string res = game.NewCommand(commandStr);
            Assert.AreEqual(null, res);
        }

        [TestMethod]
        public void StartGame()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,1,NORTH");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,NORTH", res);
        }

        [TestMethod]
        public void DontFall()
        {
            Game game = new Game();
            game.NewCommand("PLACE 0,0,SOUTH");
            game.NewCommand("MOVE");
            string res = game.NewCommand("REPORT");
            Assert.AreEqual("0,0,SOUTH", res);

            game.NewCommand("RIGHT");
            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("0,0,WEST", res);

            game.NewCommand("PLACE 4,4,NORTH");
            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("4,4,NORTH", res);

            game.NewCommand("RIGHT");
            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("4,4,EAST", res);
        }

        [TestMethod]
        public void Diagonal()
        {
            Game game = new Game();
            game.NewCommand("PLACE 0,0,NORTH");
            game.NewCommand("MOVE");
            string res = game.NewCommand("REPORT");
            Assert.AreEqual("0,1,NORTH", res);

            game.NewCommand("RIGHT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("0,1,EAST", res);

            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,EAST", res);

            game.NewCommand("LEFT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,NORTH", res);


            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("1,2,NORTH", res);

            game.NewCommand("RIGHT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("1,2,EAST", res);

            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("2,2,EAST", res);

            game.NewCommand("LEFT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("2,2,NORTH", res);


            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("2,3,NORTH", res);

            game.NewCommand("RIGHT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("2,3,EAST", res);

            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("3,3,EAST", res);

            game.NewCommand("LEFT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("3,3,NORTH", res);


            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("3,4,NORTH", res);

            game.NewCommand("RIGHT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("3,4,EAST", res);

            game.NewCommand("MOVE");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("4,4,EAST", res);

            game.NewCommand("LEFT");
            res = game.NewCommand("REPORT");
            Assert.AreEqual("4,4,NORTH", res);
        }


        [TestMethod]
        public void RoundAndRound_Clockwise()
        {
            Game game = new Game();
            game.NewCommand("PLACE 0,0,NORTH");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("RIGHT");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("RIGHT");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("RIGHT");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            string res = game.NewCommand("REPORT");
            Assert.AreEqual("0,0,WEST", res);
        }

        [TestMethod]
        public void RoundAndRound_CounterClockwise()
        {
            Game game = new Game();
            game.NewCommand("PLACE 0,0,EAST");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("LEFT");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("LEFT");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("LEFT");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            string res = game.NewCommand("REPORT");
            Assert.AreEqual("0,0,SOUTH", res);
        }
    }
}
