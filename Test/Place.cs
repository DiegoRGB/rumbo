﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rumbo;

namespace Test
{
    [TestClass]
    public class Place
    {
        [TestMethod]
        public void Command_UNK()
        {
            string commandStr = "AAA";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR_x_pos()
        {
            string commandStr = "PLACE 5,1,NORTH";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR_x_neg()
        {
            string commandStr = "PLACE -1,1,NORTH";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR_y_pos()
        {
            string commandStr = "PLACE 1,5,NORTH";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR_y_neg()
        {
            string commandStr = "PLACE 1,-1,NORTH";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR_facing()
        {
            string commandStr = "PLACE 1,1,N";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Sintax_OK()
        {
            string commandStr = "PLACE 1,1,WEST";
            bool res = CommandPlace.CheckCommand(commandStr, out CommandPlace command);
            Assert.AreEqual(true, res);
        }

        [TestMethod]
        public void Place_OK()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,1,NORTH");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,NORTH", res);
        }
    }
}
