﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rumbo;

namespace Test
{
    [TestClass]
    public class Examples
    {
        [TestMethod]
        public void A()
        {
            Game game = new Game();
            game.NewCommand("PLACE 0,0,NORTH");
            game.NewCommand("MOVE");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("0,1,NORTH", res);
        }

        [TestMethod]
        public void B()
        {
            Game game = new Game();
            game.NewCommand("PLACE 0,0,NORTH");
            game.NewCommand("LEFT");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("0,0,WEST", res);
        }

        [TestMethod]
        public void C()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,2,EAST");
            game.NewCommand("MOVE");
            game.NewCommand("MOVE");
            game.NewCommand("LEFT");
            game.NewCommand("MOVE");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("3,3,NORTH", res);
        }
    }
}
