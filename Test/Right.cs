﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rumbo;

namespace Test
{
    [TestClass]
    public class Right
    {
        [TestMethod]
        public void Command_UNK()
        {
            string commandStr = "AAA";
            bool res = CommandRight.CheckCommand(commandStr, out CommandRight command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR()
        {
            string commandStr = "RIGHT 6";
            bool res = CommandRight.CheckCommand(commandStr, out CommandRight command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Sintax_OK()
        {
            string commandStr = "RIGHT";
            bool res = CommandRight.CheckCommand(commandStr, out CommandRight command);
            Assert.AreEqual(true, res);
        }

        [TestMethod]
        public void Right_x1()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,1,NORTH");
            game.NewCommand("RIGHT");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,EAST", res);
        }

        [TestMethod]
        public void Right_x2()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,1,NORTH");
            game.NewCommand("RIGHT");
            game.NewCommand("RIGHT");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,SOUTH", res);
        }

        [TestMethod]
        public void Right_x3()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,1,NORTH");
            game.NewCommand("RIGHT");
            game.NewCommand("RIGHT");
            game.NewCommand("RIGHT");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,WEST", res);
        }

        [TestMethod]
        public void Right_x4()
        {
            Game game = new Game();
            game.NewCommand("PLACE 1,1,NORTH");
            game.NewCommand("RIGHT");
            game.NewCommand("RIGHT");
            game.NewCommand("RIGHT");
            game.NewCommand("RIGHT");

            string res = game.NewCommand("REPORT");
            Assert.AreEqual("1,1,NORTH", res);
        }
    }
}
