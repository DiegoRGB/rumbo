﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rumbo;

namespace Test
{
    [TestClass]
    public class Report
    {
        [TestMethod]
        public void Command_UNK()
        {
            string commandStr = "AAA";
            bool res = CommandReport.CheckCommand(commandStr, out CommandReport command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Syntax_ERR()
        {
            string commandStr = "REPORT 6";
            bool res = CommandReport.CheckCommand(commandStr, out CommandReport command);
            Assert.AreEqual(false, res);
        }

        [TestMethod]
        public void Sintax_OK()
        {
            string commandStr = "REPORT";
            bool res = CommandReport.CheckCommand(commandStr, out CommandReport command);
            Assert.AreEqual(true, res);
        }
    }
}
